package com.montymobile.codingchallenge.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.montymobile.codingchallenge.R
import com.montymobile.codingchallenge.databinding.ActivityLoginSignupBinding

class LoginSignupActivity : AppCompatActivity() {
    private lateinit var loginSignupBinding: ActivityLoginSignupBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginSignupBinding = ActivityLoginSignupBinding.inflate(layoutInflater)
        setContentView(loginSignupBinding.root)

        setupNavHostWithBottomNav()
    }

    /*SETUP NAV HOST FRAGMENT WITH BOTTOM NAVIGATION VIEW*/
    private fun setupNavHostWithBottomNav()
    {
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
        loginSignupBinding.bottomNavView.setupWithNavController(navHostFragment.navController)
    }
}