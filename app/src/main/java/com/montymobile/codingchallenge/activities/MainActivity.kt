package com.montymobile.codingchallenge.activities

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import com.montymobile.codingchallenge.Constants
import com.montymobile.codingchallenge.R
import com.montymobile.codingchallenge.databinding.ActivityMainBinding
import com.montymobile.codingchallenge.utils.KeyUtils
import com.montymobile.codingchallenge.utils.NotificationUtils
import com.montymobile.codingchallenge.utils.SharedPreferenceUtils

class MainActivity : AppCompatActivity(), View.OnClickListener {

    var timerCount:Long=Constants.TIMER_COUNT_10_SECONS
    var isLoggedout = false
    private lateinit var countDownTimer: CountDownTimer
    private lateinit var activityMainBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(activityMainBinding.root)

        setupUIData()
        setupListeners()
    }

    override fun onResume() {
        super.onResume()
        timerCount = Constants.TIMER_COUNT_30_SECONS
        startTimer(timerCount)
    }

    /*SETUP LISTENERS*/
    private fun setupListeners()
    {
        activityMainBinding.buttonLogout.setOnClickListener(this)
    }

    /*SHOW NOTIFICATION IF API VERSION 29 OR HIGHER DUE TO FOREGROUND ACTIVITY LAUNCH RESTRICTIONS*/
    /*ACTIVITY WILL LAUNCH BUT NOT AUTO SHOW IF APP IN BACKGROUND*/
    /*SO USER WILL BE NOTIFIED TO OPEN APP*/
    private fun checkIfNotificationRequired()
    {
        if((timerCount==Constants.TIMER_COUNT_10_SECONS) && (Build.VERSION.SDK_INT>=Build.VERSION_CODES.Q))
        {
            NotificationUtils.generateNotification(this,1003)
        }
    }

    /*LOGOUT*/
    private fun lougout()
    {
        isLoggedout = true
        checkIfNotificationRequired()
        var intent = Intent(this,LoginSignupActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    /*STOP TIMER IF ALREADY RUNNING*/
    private fun stopTimer()
    {
        if(::countDownTimer.isInitialized)
        {
            countDownTimer.cancel()
        }
    }

    /*SETUP AND START TIMER*/
    private fun startTimer(millisInFuture:Long)
    {
        stopTimer()
        countDownTimer = object:CountDownTimer(millisInFuture,1000)
        {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                lougout()
            }
        }
        countDownTimer.start()
    }

    /*SETUP UI DATA*/
    private fun setupUIData()
    {
        activityMainBinding.textViewTitle.text = resources.getString(R.string.text_welcome)+SharedPreferenceUtils
            .readUserObject(this,KeyUtils.USER_OBJECT)?.fullName
    }

    override fun onPause() {
        super.onPause()
        if(!isLoggedout) {
            timerCount = Constants.TIMER_COUNT_10_SECONS
            startTimer(timerCount)
        }
    }

    override fun onDestroy() {
        stopTimer()
        super.onDestroy()
    }

    override fun onClick(v: View) {
        when(v.id)
        {
            R.id.buttonLogout -> {
                lougout()
            }
        }
    }
}