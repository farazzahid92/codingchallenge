package com.montymobile.codingchallenge.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.montymobile.codingchallenge.R
import com.montymobile.codingchallenge.activities.MainActivity
import com.montymobile.codingchallenge.databinding.FragmentLoginBinding
import com.montymobile.codingchallenge.utils.KeyUtils
import com.montymobile.codingchallenge.utils.MessagesUtils
import com.montymobile.codingchallenge.utils.SharedPreferenceUtils
import com.montymobile.codingchallenge.utils.ValidationUtils
import com.montymobile.videorbt.utils.KeyboardUtils

class LoginFragment : Fragment(),View.OnClickListener {

    private lateinit var mContext: Context
    private lateinit var loginBinding: FragmentLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        loginBinding = FragmentLoginBinding.inflate(inflater,container,false)
        mContext = loginBinding.root.context
        return loginBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupListeners()
    }

    /*SETUP LISTENERS*/
    private fun setupListeners()
    {
        loginBinding.buttonLogin.setOnClickListener(this)
    }

    /*OPEN MAIN ACTIVITY*/
    private fun openMainActivity()
    {
        KeyboardUtils.hideSoftKeyboard(mContext,loginBinding.root)
        if(verifyData()) {
            var intent = Intent(mContext, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }



    /*CHECK IF USER REGISTERED*/
    private fun verifyCredentials():Boolean
    {
        var userBO = SharedPreferenceUtils.readUserObject(mContext, KeyUtils.USER_OBJECT)
        if(userBO!=null)
        {
            if(userBO.userName.equals(loginBinding.editTextUserName.text.toString().trim())
                && userBO.password.equals(loginBinding.editTextPassword.text.toString().trim()))
            {
                return true
            }
        }
        return false
    }

    /*VERIFY DATA ENTERED*/
    private fun verifyData():Boolean
    {
        var check = true

        if(ValidationUtils.isStringEmpty(loginBinding.editTextUserName.text.toString().trim()))
        {
            check = false
            MessagesUtils.showErrorMessageWithSnackbar(loginBinding.root,resources.getString(R.string.error_user_name_required))
        }
        else if(ValidationUtils.isStringEmpty(loginBinding.editTextPassword.text.toString().trim()))
        {
            check = false
            MessagesUtils.showErrorMessageWithSnackbar(loginBinding.root,resources.getString(R.string.error_password_required))
        }
        else if(!ValidationUtils.isPasswordValid(loginBinding.editTextPassword.text.toString().trim()))
        {
            check = false
            MessagesUtils.showErrorMessageWithSnackbar(loginBinding.root,resources.getString(R.string.error_password_length))
        }
        else if(!verifyCredentials())
        {
            check = false
            MessagesUtils.showErrorMessageWithSnackbar(loginBinding.root,resources.getString(R.string.error_user_not_registered))
        }
        return check
    }

    override fun onClick(v: View) {
        when(v.id)
        {
            R.id.buttonLogin-> openMainActivity()
        }
    }
}