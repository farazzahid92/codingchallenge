package com.montymobile.codingchallenge.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.montymobile.codingchallenge.R
import com.montymobile.codingchallenge.databinding.FragmentSignupBinding
import com.montymobile.codingchallenge.models.UserBO
import com.montymobile.codingchallenge.utils.KeyUtils
import com.montymobile.codingchallenge.utils.MessagesUtils
import com.montymobile.codingchallenge.utils.SharedPreferenceUtils
import com.montymobile.codingchallenge.utils.ValidationUtils
import com.montymobile.videorbt.utils.KeyboardUtils

class SignupFragment : Fragment(),View.OnClickListener {

    private lateinit var mContext: Context
    private lateinit var fragmentSignupBinding: FragmentSignupBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragmentSignupBinding = FragmentSignupBinding.inflate(inflater,container,false)
        mContext = fragmentSignupBinding.root.context
        return fragmentSignupBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupListeners()
    }

    /*SETUP LISTENERS*/
    private fun setupListeners()
    {
        fragmentSignupBinding.buttonRegister.setOnClickListener(this)
    }

    /*SAVE USER CREDENTIALS IN SHARED PREFERENCES*/
    private fun saveCredentials()
    {
        KeyboardUtils.hideSoftKeyboard(mContext,fragmentSignupBinding.root)
        if(verifyData())
        {
            SharedPreferenceUtils.saveUserObject(
                mContext,
                KeyUtils.USER_OBJECT,
                UserBO(
                    fragmentSignupBinding.editTextFullName.text.toString().trim(),
                    fragmentSignupBinding.editTextUserName.text.toString().trim(),
                    fragmentSignupBinding.editTextPassword.text.toString().trim()
                )
            )
            MessagesUtils.showSuccessMessageWithSnackbar(
                fragmentSignupBinding.root,resources.getString(R.string.registered_successfully))

        }
    }



    /*CHECK IF USER ALREADY REGISTERED*/
    private fun checkIfUserAlreadyRegistered():Boolean
    {
        var userBO = SharedPreferenceUtils.readUserObject(mContext,KeyUtils.USER_OBJECT)
        if(userBO!=null)
        {
            if(userBO.fullName.equals(fragmentSignupBinding.editTextFullName.text.toString().trim())
                && userBO.userName.equals(fragmentSignupBinding.editTextUserName.text.toString().trim())
                && userBO.password.equals(fragmentSignupBinding.editTextPassword.text.toString().trim()))
            {
                return true
            }
        }
        return false
    }

    /*VERIFY DATA ENTERED*/
    private fun verifyData():Boolean
    {
        var check = true

        if(checkIfUserAlreadyRegistered())
        {
            check = false
            MessagesUtils.showErrorMessageWithSnackbar(fragmentSignupBinding.root,resources.getString(R.string.error_already_registered))
        }
        else if(ValidationUtils.isStringEmpty(fragmentSignupBinding.editTextFullName.text.toString().trim()))
        {
            check = false
            MessagesUtils.showErrorMessageWithSnackbar(fragmentSignupBinding.root,resources.getString(R.string.error_full_name_required))
        }
        else if(ValidationUtils.isStringEmpty(fragmentSignupBinding.editTextUserName.text.toString().trim()))
        {
            check = false
            MessagesUtils.showErrorMessageWithSnackbar(fragmentSignupBinding.root,resources.getString(R.string.error_user_name_required))
        }
        else if(ValidationUtils.isStringEmpty(fragmentSignupBinding.editTextPassword.text.toString().trim()))
        {
            check = false
            MessagesUtils.showErrorMessageWithSnackbar(fragmentSignupBinding.root,resources.getString(R.string.error_password_required))
        }
        else if(!ValidationUtils.isPasswordValid(fragmentSignupBinding.editTextPassword.text.toString().trim()))
        {
            check = false
            MessagesUtils.showErrorMessageWithSnackbar(fragmentSignupBinding.root,resources.getString(R.string.error_password_length))
        }
        return check
    }

    override fun onClick(v: View) {
        when(v.id)
        {
            R.id.buttonRegister -> saveCredentials()
        }
    }
}