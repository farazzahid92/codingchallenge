package com.montymobile.codingchallenge.utils

import android.content.Context
import android.content.Context.MODE_PRIVATE
import com.google.gson.Gson
import com.montymobile.codingchallenge.Constants
import com.montymobile.codingchallenge.models.UserBO

object SharedPreferenceUtils
{
    /*SAVE USER OBJECT IN SHARED PREFERENCES*/
    public fun saveUserObject(context: Context,keyString: String,value:UserBO)
    {
        context.getSharedPreferences(Constants.LOCAL_STORAGE_NAME,MODE_PRIVATE).edit().apply()
        {
            putString(keyString, Gson().toJson(value))
            apply()
        }
    }

    /*READ USER OBJECT FROM SHARED PREFERENCES*/
    public fun readUserObject(context: Context,keyString: String):UserBO?
    {
        var sharedPreferences = context.getSharedPreferences(Constants.LOCAL_STORAGE_NAME,MODE_PRIVATE)
        if(sharedPreferences.contains(keyString))
        {
            return Gson().fromJson(
                sharedPreferences.getString(keyString,""),
                UserBO::class.java
            )
        }
        return null
    }
}