package com.montymobile.codingchallenge.utils

object ValidationUtils {

    /*CHECK IF STRING IS NOT EMPTY*/
    public fun isStringEmpty(string: String): Boolean {
        return string.isNullOrEmpty()
    }

    /*CHECK IF PASSWORD IS VALID*/
    public fun isPasswordValid(password: String): Boolean {
        return (password.length>=4 && password.length<8)
    }
}