package com.montymobile.codingchallenge.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import com.montymobile.codingchallenge.R
import com.montymobile.codingchallenge.activities.LoginSignupActivity

object NotificationUtils
{
    /*GENERATE NOTIFICATION IF API IS 29 OR HIGHER*/
    /*SO USER WILL BE NOTIFIED IF APP IS IN BACKGROUND*/
    public fun generateNotification(context: Context,notificationId:Int)
    {
        var notificationManager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        var builder = NotificationCompat.Builder(context)
        builder.setContentTitle(context.resources.getString(R.string.app_name))
        builder.setAutoCancel(true)
        builder.setOngoing(false)
        builder.setContentText(context.resources.getString(R.string.open))
        builder.setContentIntent(PendingIntent.getActivity(context,0, Intent(context,LoginSignupActivity::class.java),PendingIntent.FLAG_UPDATE_CURRENT))
        builder.setSmallIcon(R.drawable.ic_launcher_background)

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O)
        {
            var notificationChannel = NotificationChannel(
                context.resources.getString(R.string.app_name),
                context.resources.getString(R.string.app_name)
                ,NotificationManager.IMPORTANCE_HIGH)
            builder.setChannelId(context.resources.getString(R.string.app_name))
            notificationManager.createNotificationChannel(notificationChannel)
        }

        notificationManager.notify(notificationId,builder.build())
    }
}