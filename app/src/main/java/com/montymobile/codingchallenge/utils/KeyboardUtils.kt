package com.montymobile.videorbt.utils

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

object KeyboardUtils {
    /*HIDE SOFT KEYBOARD*/
    public fun hideSoftKeyboard(context: Context, view: View) {
        (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
            view.getApplicationWindowToken(),
            0
        )
    }
}