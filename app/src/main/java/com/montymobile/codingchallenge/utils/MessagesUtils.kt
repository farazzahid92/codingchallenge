package com.montymobile.codingchallenge.utils

import android.view.View
import com.google.android.material.snackbar.Snackbar

object MessagesUtils
{
    /*SHOW ERROR MESSAGE*/
    fun showErrorMessageWithSnackbar(view: View,message:String)
    {
        Snackbar.make(view,message,Snackbar.LENGTH_LONG).show()
    }

    /*SHOW SUCCESS MESSAGE*/
    fun showSuccessMessageWithSnackbar(view: View,message:String)
    {
        Snackbar.make(view,message,Snackbar.LENGTH_LONG).show()
    }
}