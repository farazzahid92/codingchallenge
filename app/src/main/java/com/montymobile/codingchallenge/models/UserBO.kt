package com.montymobile.codingchallenge.models

data class UserBO(
    var fullName:String,
    var userName:String,
    var password:String
)