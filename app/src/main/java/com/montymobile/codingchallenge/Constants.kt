package com.montymobile.codingchallenge

object Constants
{
    const val LOCAL_STORAGE_NAME="LOCAL_STORAGE_NAME"
    const val TIMER_COUNT_30_SECONS:Long=30000
    const val TIMER_COUNT_10_SECONS:Long=10000
}