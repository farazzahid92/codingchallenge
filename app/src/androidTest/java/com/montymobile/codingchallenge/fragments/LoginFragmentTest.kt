package com.montymobile.codingchallenge.fragments

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.montymobile.codingchallenge.R
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

//THIS IS INTEGRATION TEST FOR LOGIN FRAGMENT
@RunWith(AndroidJUnit4::class)
class LoginFragmentTest
{
    private lateinit var scenario: FragmentScenario<LoginFragment>

    //THIS METHOD WILL EXECUTE BEFORE TEST CASE
    //IT WILL LAUNCH LOGIN FRAGMENT IN CONTAINER WITH APP THEME FOR TEST AND MOVE IT TO STARTED STATE
    @Before
    fun setup()
    {
        scenario = launchFragmentInContainer(themeResId = R.style.Theme_MaterialComponents_DayNight_NoActionBar)
        scenario.moveToState(Lifecycle.State.STARTED)
    }

    //ON BUTTON CLICK IT WILL CHECK IF USER REGISTERED WITH WRONG CREDENTIALS
    @Test
    fun testUserLogin()
    {
        onView(ViewMatchers.withId(R.id.editTextUserName))
            .perform(ViewActions.typeText("zahid meh"))
        onView(ViewMatchers.withId(R.id.editTextPassword))
            .perform(ViewActions.typeText("1234"))
        Espresso.closeSoftKeyboard()
        onView(ViewMatchers.withId(R.id.buttonLogin)).perform(ViewActions.click())
    }

    //ON LOGIN BUTTON CLICK IT WILL CHECK IF USER ALREADY REGISTERED
    @Test
    fun testUserAlreadyAdded()
    {
        onView(ViewMatchers.withId(R.id.editTextUserName))
            .perform(ViewActions.typeText("zahid"))
        onView(ViewMatchers.withId(R.id.editTextPassword))
            .perform(ViewActions.typeText("1234"))
        Espresso.closeSoftKeyboard()
        onView(ViewMatchers.withId(R.id.buttonLogin)).perform(ViewActions.click())
    }
}