package com.montymobile.codingchallenge.fragments

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.montymobile.codingchallenge.R
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

//THIS IS INTEGRATION TEST FOR SIGNUP FRAGMENT
@RunWith(AndroidJUnit4::class)
class SignupFragmentTest
{
    private lateinit var scenario:FragmentScenario<SignupFragment>

    //THIS METHOD WILL EXECUTE BEFORE TEST CASE
    //IT WILL LAUNCH SIGNUP FRAGMENT IN CONTAINER WITH APP THEME FOR TEST AND MOVE IT TO STARTED STATE
    @Before
    fun setup()
    {
        scenario = launchFragmentInContainer(themeResId = R.style.Theme_MaterialComponents_DayNight_NoActionBar)
        scenario.moveToState(Lifecycle.State.STARTED)
    }

    //ON REGISTER BUTTON CLICK IT WILL CHECK IF USER ADDED
    @Test
    fun testUserAdded()
    {
        onView(withId(R.id.editTextFullName)).perform(typeText("farazgg"))
        onView(withId(R.id.editTextUserName)).perform(typeText("zahidkk"))
        onView(withId(R.id.editTextPassword)).perform(typeText("12345"))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.buttonRegister)).perform(click())
    }

    //ON REGISTER BUTTON CLICK IT WILL CHECK IF USER ALREADY ADDED
    @Test
    fun testUserAlreadyAdded()
    {
        onView(withId(R.id.editTextFullName)).perform(typeText("farazgg"))
        onView(withId(R.id.editTextUserName)).perform(typeText("zahidkk"))
        onView(withId(R.id.editTextPassword)).perform(typeText("12345"))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.buttonRegister)).perform(click())
    }
}