package com.montymobile.codingchallenge.activities

import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.montymobile.codingchallenge.R
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

//THIS IS INTEGRATION TEST FOR MAIN ACTIVITY
@RunWith(AndroidJUnit4::class)
class MainActivityTest
{
    private lateinit var scenario: ActivityScenario<MainActivity>

    //THIS METHOD WILL EXECUTE BEFORE TEST CASE
    //IT WILL LAUNCH ACTIVITY FOR TEST AND MOVE IT TO RESUMED STATE
    @Before
    fun setup()
    {
        scenario = ActivityScenario.launch(MainActivity::class.java)
        scenario.moveToState(Lifecycle.State.RESUMED)
    }

    //TEST CASE TO CLICK ON LOGOUT BUTTON
    //ALL CODE IN MAIN ACTIVITY ON BUTTON CLICK WILL LAUNCH
    //IF THERE WILL BE ERROR IN ACTIVITY CODE TEST FAILS
    @Test
    fun checkButtonClick()
    {
        onView(withId(R.id.buttonLogout)).perform(click())
    }
}