package com.montymobile.codingchallenge.utils

import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import com.montymobile.codingchallenge.models.UserBO
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

//THIS IS UNIT TEST
@RunWith(AndroidJUnit4::class)
class SharedPreferenceUtilsTest
{
    //THIS METHOD WILL EXECUTE BEFORE TEST CASE
    //IT WILL ADD USER TO LOCAL STORAGE
    @Before
    fun insertUser()
    {
        SharedPreferenceUtils
            .saveUserObject(
                ApplicationProvider.getApplicationContext(),
                KeyUtils.USER_OBJECT,
                UserBO("faraz","zahid","1234")
            )
    }

    //THIS METHOD WILL CHECK IF USER ADDEDD SUCCESSFULLY
    @Test
    fun checkIfUserAdded()
    {
        assertThat(SharedPreferenceUtils
            .readUserObject(
                ApplicationProvider.getApplicationContext(),
                KeyUtils.USER_OBJECT
            )?.fullName.equals("faraz")).isTrue()
    }
}